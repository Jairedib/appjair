package Vista;

import javax.swing.JFrame;
import javax.swing.JTextField;

import modelo.ClasesGym;
import modelo.Trabajador;

import javax.swing.JComboBox;
import javax.swing.JButton;
import javax.swing.JLabel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.SwingConstants;
import javax.swing.JScrollPane;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.JSpinner;
import javax.swing.ImageIcon;
public class Vistaregistrado extends JFrame {
		public JTextField tffechaini;
		public JTextField tffechafin;
		 public JButton btnGuardar;
		 public JButton btnReset;
		 private JLabel lblNewLabel_1;
		 public JTextField tfclase;
		 public JTextField tfcliente;
		 public JScrollPane scrollPane;
		 
		
		
		public Vistaregistrado() {
			getContentPane().setLayout(null);
			setVisible(true);
			revalidate();
			repaint();
			setSize(395,549);
			setLocationRelativeTo(null);
			
			tffechaini = new JTextField();
			tffechaini.setEditable(false);
			tffechaini.setBounds(52, 126, 116, 19);
			getContentPane().add(tffechaini);
			tffechaini.setColumns(10);
			
			tffechafin = new JTextField();
			tffechafin.setEditable(false);
			tffechafin.setBounds(191, 126, 130, 19);
			getContentPane().add(tffechafin);
			tffechafin.setColumns(10);
			
			btnGuardar = new JButton("Guardar");
			btnGuardar.setIcon(new ImageIcon(Vistaregistrado.class.getResource("/resources/salvar.png")));
			btnGuardar.setBounds(52, 186, 104, 21);
			getContentPane().add(btnGuardar);
			
			 btnReset = new JButton("Reset");
			 btnReset.setIcon(new ImageIcon(Vistaregistrado.class.getResource("/resources/baseline_update_black_18dp.png")));
			btnReset.setBounds(177, 186, 89, 21);
			getContentPane().add(btnReset);
			
			JLabel lblNewLabel = new JLabel("ID clase");
			lblNewLabel.setBounds(52, 38, 45, 13);
			getContentPane().add(lblNewLabel);
			
			JLabel lblNewLabel_2 = new JLabel("Fecha de Inicio");
			lblNewLabel_2.setBounds(52, 107, 72, 13);
			getContentPane().add(lblNewLabel_2);
			
			JLabel lblNewLabel_3 = new JLabel("Hora");
			lblNewLabel_3.setBounds(191, 107, 75, 13);
			getContentPane().add(lblNewLabel_3);
			
			JPanel RegistPanel = new JPanel();
			RegistPanel.setBounds(10, 254, 361, 248);
			getContentPane().add(RegistPanel);
			RegistPanel.setLayout(new BorderLayout(0, 0));
			
			scrollPane = new JScrollPane();
			RegistPanel.add(scrollPane, BorderLayout.CENTER);
			
			lblNewLabel_1 = new JLabel("ID Cliente");
			lblNewLabel_1.setBounds(191, 38, 45, 13);
			getContentPane().add(lblNewLabel_1);
			
			tfclase = new JTextField();
			tfclase.setEditable(false);
			tfclase.setBounds(52, 61, 116, 19);
			getContentPane().add(tfclase);
			tfclase.setColumns(10);
			
			tfcliente = new JTextField();
			tfcliente.setEditable(false);
			tfcliente.setBounds(191, 61, 130, 19);
			getContentPane().add(tfcliente);
			tfcliente.setColumns(10);
		}
	}

