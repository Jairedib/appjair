package Vista;

import java.awt.BorderLayout;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JToolBar;

import Componentes.Tabla;
import varios.Datos;

import java.awt.event.ActionListener;
import java.util.LinkedList;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.ImageIcon;

public class ClientePanel extends JPanel {
	
	//private static final long serialVersionUID = 1L;
	public Tabla tablaclientes;
	
	public ClientePanel() {
		setLayout(new BorderLayout(0, 0));
		JToolBar toolBar = new JToolBar();
		this.add(toolBar, BorderLayout.NORTH);
		
		
		JButton btnbuscar = new JButton("Buscar");
		btnbuscar.setIcon(new ImageIcon(ClientePanel.class.getResource("/resources/buscar.png")));
		btnbuscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				BusquedaClientes b= new BusquedaClientes();
				b.getRootPane().setDefaultButton(b.btnBuscar);
				
				b.btnBuscar.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						
						Integer i= ((Integer)b.SPidcliente.getValue())==0? null:(Integer)b.SPidcliente.getValue();
						String n=b.TFnombre.getText().isEmpty()? null:b.TFnombre.getText();
						String ap= b.TFapellidos.getText().isEmpty()? null:b.TFapellidos.getText();
						String dni= b.tfdni.getText().isEmpty()? null: b.tfdni.getText();
						
						LinkedList<modelo.Cliente> lista = modelo.Cliente.find(i, n, ap, dni);
						tablaclientes.vaciar();
						for (modelo.Cliente c:lista) {
							Object[] fila= {c.ID,c.nombre,c.apellidos,c.telefono,c.direccion,c.email};
							tablaclientes.modelo.addRow(fila);
						
						}
						b.dispose();
					}
				});
			}
		});
		toolBar.add(btnbuscar);
		
		JButton btnNuevo = new JButton("Nuevo");
		btnNuevo.setIcon(new ImageIcon(ClientePanel.class.getResource("/resources/mas.png")));
		btnNuevo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				 new controlador.Clientecont();
			}
		});
		toolBar.add(btnNuevo);
		
		JButton btActualizar = new JButton("");
		btActualizar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				tablaclientes.vaciar();
				for (modelo.Cliente c:modelo.Cliente.find(null, null, null, null)) {
					Object[] fila= {c.ID,c.nombre,c.apellidos,c.telefono,c.direccion,c.email};
					tablaclientes.modelo.addRow(fila);
				
				}
				
			}
		});
		btActualizar.setIcon(new ImageIcon(ClientePanel.class.getResource("/resources/baseline_update_black_18dp.png")));
		toolBar.add(btActualizar);
		
		JScrollPane scrollPane = new JScrollPane();
		this.add(scrollPane);
		
		Object[] cabecera= {"id","nombre","apellidos","telefono","direccion","email"};
		Class<?>[] tipos = {Integer.class,String.class,String.class,String.class,String.class,String.class};
		Integer [] medidas= {80,100,120,90,130,130};
		//Tabla?? tablaclientes = new...?
		//La clase tabla de componentes la instanciamos e importamos, y le pasamos los valores que necesita pasarle,cabecera,tipos, medidas y colores
		tablaclientes = new Tabla(cabecera,tipos,medidas,null,null,null);
		/*Object[] fila= {32,"b","c","d","e","f"};
		tablaclientes.modelo.addRow(fila);*/
		scrollPane.setViewportView(tablaclientes.tabla);
		tablaclientes.tabla.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if(e.getClickCount()==2 && e.getButton()==1) {
					Integer id=(int) tablaclientes.getValueSelected(0);
					new controlador.Clientecont(id);
					
				}
			}
		});
		
	}

}
