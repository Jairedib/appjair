package Vista;

import javax.swing.JFrame;
import java.awt.Color;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JButton;
import javax.swing.JLabel;
import java.awt.BorderLayout;
import javax.swing.ImageIcon;
import javax.swing.ButtonGroup;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Toolkit;

public class sign extends JFrame  {
	private final ButtonGroup buttonGroup = new ButtonGroup();
	public JButton btnenter;
	
	public sign() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(sign.class.getResource("/resources/gimnasio.png")));
		setResizable(false);
		setVisible(true);
		repaint();
		revalidate();
	
		getContentPane().setBackground(Color.BLACK);
		getContentPane().setLayout(null);
		setSize(845,400);
		setLocationRelativeTo(null);
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 329, 372);
		getContentPane().add(panel);
		panel.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setBounds(0, 0, 329, 372);
		lblNewLabel.setIcon(new ImageIcon(sign.class.getResource("/resources/informatica.jpg")));
		panel.add(lblNewLabel);
		
		JRadioButton rdbtnNewRadioButton = new JRadioButton("Java Palma");
		buttonGroup.add(rdbtnNewRadioButton);
		rdbtnNewRadioButton.setForeground(Color.BLACK);
		rdbtnNewRadioButton.setBounds(365, 67, 103, 21);
		getContentPane().add(rdbtnNewRadioButton);
		
		JRadioButton rdbtnNewRadioButton_1 = new JRadioButton("Java Marratxi");
		buttonGroup.add(rdbtnNewRadioButton_1);
		rdbtnNewRadioButton_1.setBounds(365, 126, 103, 21);
		getContentPane().add(rdbtnNewRadioButton_1);
		
		JRadioButton rdbtnJavaAlcudia = new JRadioButton("Java Alcudia");
		buttonGroup.add(rdbtnJavaAlcudia);
		rdbtnJavaAlcudia.setBounds(365, 183, 103, 21);
		getContentPane().add(rdbtnJavaAlcudia);
		
		JRadioButton rdbtnNewRadioButton_1_1 = new JRadioButton("Java Cala D'or");
		buttonGroup.add(rdbtnNewRadioButton_1_1);
		rdbtnNewRadioButton_1_1.setBounds(365, 242, 103, 21);
		getContentPane().add(rdbtnNewRadioButton_1_1);
		
		 btnenter = new JButton("Enter");
		btnenter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new VistaAplicacion();
				dispose();
			}
			
			
		});
	
		btnenter.setBounds(369, 293, 92, 21);
		getContentPane().add(btnenter);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBounds(502, 0, 329, 372);
		getContentPane().add(panel_1);
		panel_1.setLayout(null);
		
		JLabel lblNewLabel_1 = new JLabel("");
		lblNewLabel_1.setBounds(0, 0, 329, 362);
		lblNewLabel_1.setIcon(new ImageIcon(sign.class.getResource("/resources/informatica.jpg")));
		panel_1.add(lblNewLabel_1);
		setBackground(Color.WHITE);
	}
}
