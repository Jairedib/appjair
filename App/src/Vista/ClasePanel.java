package Vista;

import java.awt.BorderLayout;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JToolBar;

import Componentes.Tabla;
import modelo.Trabajador;

import java.awt.event.ActionListener;
import java.util.LinkedList;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.ImageIcon;

public class ClasePanel  extends JPanel{
	public Tabla tablaclases;
	public BusquedaClases b;
	
	public ClasePanel() {
		setVisible(true);
		revalidate();
		repaint();
		
		
	setLayout(new BorderLayout(0, 0));
	JToolBar toolBar = new JToolBar();
	this.add(toolBar, BorderLayout.NORTH);
	
	JButton btnbuscar = new JButton("Buscar");
	btnbuscar.setIcon(new ImageIcon(ClasePanel.class.getResource("/resources/buscar.png")));
	btnbuscar.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			 b = new BusquedaClases();
			 rellenaprofesFromDatabase();
			
			b.getRootPane().setDefaultButton(b.btnBuscar);
			b.btnBuscar.addActionListener(new ActionListener() {
				 	public void actionPerformed(ActionEvent e) {
				 		Integer i= ((Integer)b.spID.getValue())==0? null:(Integer)b.spID.getValue();
						String n=b.tfNombre.getText().isEmpty()? null:b.tfNombre.getText();
						Integer tr=((modelo.Trabajador)b.cbProfe.getSelectedItem()).id;
						String ini= b.tffechaini.getText().isEmpty()? null:b.tffechaini.getText();
						String fin= b.tffechafin.getText().isEmpty()? null: b.tffechafin.getText();
						
						LinkedList<modelo.ClasesGym> lista = modelo.ClasesGym.find(i, tr, n, ini, fin);
						tablaclases.vaciar();
						for (modelo.ClasesGym c:lista) {
							Object[] fila= {c.id,modelo.Trabajador.load(c.idTrabajadores),c.Nombre,c.fecha_inicio,c.hora};
							tablaclases.modelo.addRow(fila);
						}
						b.dispose();
				 	}
				 	
				 });
			
			
		}
	});
	toolBar.add(btnbuscar);
	
	JButton btnNuevo = new JButton("Nuevo");
	btnNuevo.setIcon(new ImageIcon(ClasePanel.class.getResource("/resources/mas.png")));
	btnNuevo.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			new controlador.Clasescont();
		}
	});
	toolBar.add(btnNuevo);
	
	JButton btnActualizar = new JButton("");
	btnActualizar.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			tablaclases.vaciar();
			for (modelo.ClasesGym c:modelo.ClasesGym.find(null, null, null, null, null)) {
				Object[] fila= {c.id,modelo.Trabajador.load(c.idTrabajadores),c.Nombre,c.fecha_inicio,c.hora};
				tablaclases.modelo.addRow(fila);
			}
		}
	});
	btnActualizar.setIcon(new ImageIcon(ClasePanel.class.getResource("/resources/baseline_update_black_18dp.png")));
	toolBar.add(btnActualizar);
	JScrollPane scrollPane = new JScrollPane();
	this.add(scrollPane);
	
	Object[] cabecera= {"id","profesor","nombre","fecha Clase","Hora"};
	Class<?>[] tipos = {Integer.class,Integer.class,String.class,String.class,String.class};
	Integer [] medidas= {80,100,120,90};
	 tablaclases = new Tabla(cabecera,tipos,medidas,null,null,null);
	scrollPane.setViewportView(tablaclases.tabla);
	tablaclases.tabla.addMouseListener(new MouseAdapter() {
		@Override
		public void mouseClicked(MouseEvent e) {
			if(e.getClickCount()==2 && e.getButton()==1) {
				Integer id=(int) tablaclases.getValueSelected(0);
				new controlador.Registrocont(id);
				new controlador.Clasescont(id);
		
				
			}
		}
	});
	

	}
	private void rellenaprofesFromDatabase() {
		
		LinkedList<modelo.Trabajador> l=modelo.Trabajador.find(null,null,null,null);
		b.cbProfe.removeAllItems();
		b.cbProfe.addItem(new modelo.Trabajador(null,"Selecciona",null,null, null, null, null, null));
		for (modelo.Trabajador ci:l) {
			b.cbProfe.addItem(ci);
			
			}
		}

}
