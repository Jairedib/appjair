package Vista;

import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;
import java.awt.Toolkit;

public class BusquedaTrabajadores extends JFrame {
	public JTextField TFnombre;
	public JTextField TFapellidos;
	public JTextField tfdni;
	public JButton btnBuscar;
	public JSpinner SPidtrabajador;
	
	public BusquedaTrabajadores() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(BusquedaTrabajadores.class.getResource("/resources/gimnasio.png")));
		setVisible(true);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setSize(274,266);
		getContentPane().setLayout(null);
		setLocationRelativeTo(null);
		
		TFnombre = new JTextField();
		TFnombre.setBounds(10, 119, 78, 19);
		getContentPane().add(TFnombre);
		TFnombre.setColumns(10);
		
		TFapellidos = new JTextField();
		TFapellidos.setBounds(98, 119, 152, 19);
		getContentPane().add(TFapellidos);
		TFapellidos.setColumns(10);
		
		tfdni = new JTextField();
		tfdni.setBounds(98, 62, 152, 19);
		getContentPane().add(tfdni);
		tfdni.setColumns(10);
		
		SPidtrabajador = new JSpinner();
		SPidtrabajador.setModel(new SpinnerNumberModel(new Integer(0), new Integer(0), null, new Integer(1)));
		SPidtrabajador.setBounds(10, 61, 78, 20);
		getContentPane().add(SPidtrabajador);
		
		JLabel lblNewLabel = new JLabel("ID Trabajador");
		lblNewLabel.setFont(new Font("Times New Roman", Font.BOLD, 12));
		lblNewLabel.setBounds(10, 42, 78, 13);
		getContentPane().add(lblNewLabel);
		
		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setFont(new Font("Times New Roman", Font.BOLD, 12));
		lblNombre.setBounds(10, 96, 68, 13);
		getContentPane().add(lblNombre);
		
		JLabel lblApellidos = new JLabel("Apellidos");
		lblApellidos.setFont(new Font("Times New Roman", Font.BOLD, 12));
		lblApellidos.setBounds(98, 96, 68, 13);
		getContentPane().add(lblApellidos);
		
		JLabel lblDni = new JLabel("Dni");
		lblDni.setFont(new Font("Times New Roman", Font.BOLD, 12));
		lblDni.setBounds(98, 42, 68, 13);
		getContentPane().add(lblDni);
		
		btnBuscar = new JButton("Buscar");
		btnBuscar.setIcon(new ImageIcon(BusquedaTrabajadores.class.getResource("/resources/buscar.png")));
		btnBuscar.setBounds(81, 178, 85, 29);
		getContentPane().add(btnBuscar);
		repaint();
		revalidate();
		
		
	}
}
