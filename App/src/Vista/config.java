package Vista;

import javax.swing.JFrame;
import javax.swing.JTextField;

import modelo.Conexion;
import varios.Datos;

import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class config extends JFrame{
	private JTextField tfbbdd;
	private JTextField tfhost;
	private JTextField tfpass;
	private JTextField tfusuario;
	public JButton btguardar;
	
	public config() {
		setResizable(false);
		getContentPane().setLayout(null);
		setVisible(true);
		repaint();
		revalidate();
		setSize(236,315);
		setLocationRelativeTo(null);
		
		tfbbdd = new JTextField();
		tfbbdd.setBounds(37, 40, 158, 19);
		getContentPane().add(tfbbdd);
		tfbbdd.setColumns(10);
		
		tfhost = new JTextField();
		tfhost.setColumns(10);
		tfhost.setBounds(37, 87, 158, 19);
		getContentPane().add(tfhost);
		
		tfpass = new JTextField();
		tfpass.setColumns(10);
		tfpass.setBounds(37, 182, 158, 19);
		getContentPane().add(tfpass);
		
		tfusuario = new JTextField();
		tfusuario.setColumns(10);
		tfusuario.setBounds(37, 135, 158, 19);
		getContentPane().add(tfusuario);
		
		JLabel lblNewLabel = new JLabel("Base de Datos");
		lblNewLabel.setBounds(37, 17, 65, 13);
		getContentPane().add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Host");
		lblNewLabel_1.setBounds(37, 69, 45, 13);
		getContentPane().add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("Usuario");
		lblNewLabel_2.setBounds(37, 116, 45, 13);
		getContentPane().add(lblNewLabel_2);
		
		JLabel lblNewLabel_3 = new JLabel("Password");
		lblNewLabel_3.setBounds(37, 159, 45, 13);
		getContentPane().add(lblNewLabel_3);
		
		btguardar = new JButton("Guardar");
		btguardar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Conexion.bd = tfbbdd.getText();
				Conexion.host = tfhost.getText();
				Conexion.login = tfusuario.getText();
				//conexion.pass = new String (tfpass.getPassword());
				Conexion.pass =tfpass.getText();
				Conexion.guardarpropiedades();
				Datos.minfo("datos guardados");
			}
		});
		btguardar.setBounds(73, 233, 85, 21);
		getContentPane().add(btguardar);
		
		Conexion.cargarPropiedades();
		tfbbdd.setText(Conexion.bd);
		tfhost.setText(Conexion.host);
		tfusuario.setText(Conexion.login);
		tfpass.setText(Conexion.pass);
		
		
		
	}
}
