package Vista;

import java.awt.BorderLayout;
import java.awt.ScrollPane;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JToolBar;

import Componentes.Tabla;
import controlador.Trabajadorcont;

import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.LinkedList;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;

public class TrabajadorPanel extends JPanel {
	public Tabla tablatrabajador1;
	public JButton btActualizar;
	
	public TrabajadorPanel() {
		setVisible(true);
		repaint();
		revalidate();
	
		
		setLayout(new BorderLayout(0, 0));
	JToolBar toolBar = new JToolBar();
	this.add(toolBar, BorderLayout.NORTH);
	
	JButton btnbuscar = new JButton("Buscar");
	btnbuscar.setIcon(new ImageIcon(TrabajadorPanel.class.getResource("/resources/buscar.png")));
	btnbuscar.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			BusquedaTrabajadores bustrab = new BusquedaTrabajadores();
			
			bustrab.btnBuscar.addActionListener(new ActionListener() {
				
				public void actionPerformed(ActionEvent e) {
				
					Integer i = (Integer) bustrab.SPidtrabajador.getValue()==0? null:(Integer)bustrab.SPidtrabajador.getValue();
					String n = bustrab.TFnombre.getText().isEmpty()? null:bustrab.TFnombre.getText();
					String ap = bustrab.TFapellidos.getText().isEmpty()? null: bustrab.TFapellidos.getText();
					String dni = bustrab.tfdni.getText().isEmpty()? null: bustrab.tfdni.getText();
					
					
					LinkedList<modelo.Trabajador> lista = modelo.Trabajador.find(i, n, ap, dni);
					tablatrabajador1.vaciar();
					for(modelo.Trabajador t:lista) {
					
						Object[] fila = {t.id,t.nombre,t.apellidos,t.telefono,t.num_seguridad_social,t.direccion,t.Cuenta_banco,t.dni};
						tablatrabajador1.modelo.addRow(fila);
					
						
					}
					bustrab.dispose();
				}
				
			});
		}
	});
	toolBar.add(btnbuscar);
	
	JButton btnNuevo = new JButton("Nuevo");
	btnNuevo.setIcon(new ImageIcon(TrabajadorPanel.class.getResource("/resources/mas.png")));
	btnNuevo.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			 new Trabajadorcont();
		}
	});
	toolBar.add(btnNuevo);
	
	 btActualizar = new JButton("");
	btActualizar.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			tablatrabajador1.vaciar();
			for(modelo.Trabajador t:modelo.Trabajador.find(null, null, null, null)) {
			
				Object[] fila = {t.id,t.nombre,t.apellidos,t.telefono,t.num_seguridad_social,t.direccion,t.Cuenta_banco,t.dni};
				tablatrabajador1.modelo.addRow(fila);
			
				
			}
			
		}
	});
	btActualizar.setIcon(new ImageIcon(TrabajadorPanel.class.getResource("/resources/baseline_update_black_18dp.png")));
	toolBar.add(btActualizar);
	
	JScrollPane scrollPane = new JScrollPane();
	this.add(scrollPane);
	
	
	Object[] cabecera= {"Id","Nombre","Apellidos","Telefono","Numero SS","direccion","Cuenta Corriente","Dni"};
	Class<?>[] tipos= {Integer.class,String.class,String.class,String.class,String.class,String.class,String.class,String.class};
	Integer[] medidas= {40,80,95,95,100,90,100,100};
	
	 tablatrabajador1= new Tabla(cabecera, tipos, medidas, null, null, null);
	scrollPane.setViewportView(tablatrabajador1.tabla);

	tablatrabajador1.tabla.addMouseListener(new MouseAdapter() {
		@Override
		public void mouseClicked(MouseEvent e) {
			if(e.getClickCount()==2 && e.getButton()==1) {
				Integer id=(int) tablatrabajador1.getValueSelected(0);
				new controlador.Trabajadorcont(id);
	
				
			}
		}
	});
	
	

	
		}
	
	}

	
