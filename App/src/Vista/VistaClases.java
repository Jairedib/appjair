package Vista;

import javax.swing.JFrame;
import javax.swing.JTextField;

import modelo.Trabajador;

import javax.swing.JComboBox;
import javax.swing.JButton;
import javax.swing.JLabel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;

public class VistaClases extends JFrame {
	public JTextField tfNombre;
	public JTextField tffechaini;
	public JTextField tffechafin;
	 public JButton btnGuardar;
	 public JButton btnReset;
	 public JButton btnEliminar;
	 public JComboBox<modelo.Trabajador> cbProfe;
	 
	
	
	public VistaClases() {
		getContentPane().setLayout(null);
		setVisible(true);
		revalidate();
		repaint();
		setSize(285,300);
		setLocationRelativeTo(null);
		
		tfNombre = new JTextField();
		tfNombre.setBounds(149, 48, 104, 19);
		getContentPane().add(tfNombre);
		tfNombre.setColumns(10);
		
		 cbProfe = new JComboBox<modelo.Trabajador>();
		cbProfe.setBounds(10, 47, 116, 21);
		getContentPane().add(cbProfe);
		
		tffechaini = new JTextField();
		tffechaini.setBounds(10, 113, 116, 19);
		getContentPane().add(tffechaini);
		tffechaini.setColumns(10);
		
		tffechafin = new JTextField();
		tffechafin.setBounds(149, 113, 104, 19);
		getContentPane().add(tffechafin);
		tffechafin.setColumns(10);
		
		btnGuardar = new JButton("Guardar");
		btnGuardar.setIcon(new ImageIcon(VistaClases.class.getResource("/resources/salvar.png")));
		btnGuardar.setBounds(29, 166, 97, 21);
		getContentPane().add(btnGuardar);
		
		 btnReset = new JButton("Reset");
		 btnReset.setIcon(new ImageIcon(VistaClases.class.getResource("/resources/baseline_update_black_18dp.png")));
		btnReset.setBounds(89, 211, 93, 21);
		getContentPane().add(btnReset);
		
		 btnEliminar = new JButton("Eliminar");
		 btnEliminar.setIcon(new ImageIcon(VistaClases.class.getResource("/resources/basura.png")));
		
		btnEliminar.setBounds(149, 166, 97, 21);
		getContentPane().add(btnEliminar);
		
		JLabel lblNewLabel = new JLabel("Profesor");
		lblNewLabel.setBounds(10, 25, 45, 13);
		getContentPane().add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Clase");
		lblNewLabel_1.setBounds(149, 25, 45, 13);
		getContentPane().add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("Fecha de Inicio");
		lblNewLabel_2.setBounds(10, 94, 72, 13);
		getContentPane().add(lblNewLabel_2);
		
		JLabel lblNewLabel_3 = new JLabel("Hora");
		lblNewLabel_3.setBounds(149, 94, 75, 13);
		getContentPane().add(lblNewLabel_3);
	}
}
