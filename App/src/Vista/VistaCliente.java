package Vista;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JToolBar;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.ImageIcon;

public class VistaCliente extends JFrame {
	public JTextField TFNombre;
	public JTextField TFApellidos;
	public JTextField TFTelefono;
	public JTextField TFDireccion;
	public JTextField TFPoblacion;
	public JTextField TFEmail;
	private JPanel contentPane;
	public JButton btGuardar;
	public JButton btnEliminar;
	public JLabel lbIDE;
	public JButton btnReset;

	
	public VistaCliente() {
		setVisible(true);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 339, 364);
		setLocationRelativeTo(null);
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(null);
		setContentPane(contentPane);
		TFNombre = new JTextField();
		TFNombre.setHorizontalAlignment(SwingConstants.CENTER);
		TFNombre.setBounds(10, 70, 142, 19);
		contentPane.add(TFNombre);
		TFNombre.setColumns(10);
		
		TFApellidos = new JTextField();
		TFApellidos.setHorizontalAlignment(SwingConstants.CENTER);
		TFApellidos.setBounds(162, 70, 144, 19);
		contentPane.add(TFApellidos);
		TFApellidos.setColumns(10);
		
		TFTelefono = new JTextField();
		TFTelefono.setHorizontalAlignment(SwingConstants.CENTER);
		TFTelefono.setBounds(10, 138, 142, 19);
		contentPane.add(TFTelefono);
		TFTelefono.setColumns(10);
		
		TFDireccion = new JTextField();
		TFDireccion.setHorizontalAlignment(SwingConstants.CENTER);
		TFDireccion.setBounds(162, 138, 144, 19);
		contentPane.add(TFDireccion);
		TFDireccion.setColumns(10);
		
		TFPoblacion = new JTextField();
		TFPoblacion.setHorizontalAlignment(SwingConstants.CENTER);
		TFPoblacion.setColumns(10);
		TFPoblacion.setBounds(10, 202, 142, 19);
		contentPane.add(TFPoblacion);
		
		TFEmail = new JTextField();
		TFEmail.setHorizontalAlignment(SwingConstants.CENTER);
		TFEmail.setColumns(10);
		TFEmail.setBounds(162, 202, 144, 19);
		contentPane.add(TFEmail);
		
		JLabel lblNewLabel = new JLabel("Nombre");
		lblNewLabel.setFont(new Font("Times New Roman", Font.BOLD, 12));
		lblNewLabel.setBounds(10, 47, 45, 13);
		contentPane.add(lblNewLabel);
		
		JLabel lblApellidos = new JLabel("Apellidos");
		lblApellidos.setFont(new Font("Times New Roman", Font.BOLD, 12));
		lblApellidos.setBounds(162, 47, 55, 13);
		contentPane.add(lblApellidos);
		
		JLabel LBLtelefono = new JLabel("Telefono");
		LBLtelefono.setFont(new Font("Times New Roman", Font.BOLD, 12));
		LBLtelefono.setBounds(10, 116, 79, 13);
		contentPane.add(LBLtelefono);
		
		JLabel lblDireccion = new JLabel("Direccion");
		lblDireccion.setFont(new Font("Times New Roman", Font.BOLD, 12));
		lblDireccion.setBounds(162, 116, 55, 13);
		contentPane.add(lblDireccion);
		
		JLabel lblPoblacion = new JLabel("Dni");
		lblPoblacion.setFont(new Font("Times New Roman", Font.BOLD, 12));
		lblPoblacion.setBounds(10, 179, 68, 13);
		contentPane.add(lblPoblacion);
		
		JLabel lblEmail = new JLabel("Email");
		lblEmail.setFont(new Font("Times New Roman", Font.BOLD, 12));
		lblEmail.setBounds(162, 179, 45, 13);
		contentPane.add(lblEmail);
		
		btGuardar = new JButton("Guardar");
		btGuardar.setIcon(new ImageIcon(VistaCliente.class.getResource("/resources/salvar.png")));
		btGuardar.setBounds(10, 248, 96, 27);
		contentPane.add(btGuardar);
		
		 lbIDE = new JLabel("");
		lbIDE.setHorizontalAlignment(SwingConstants.RIGHT);
		lbIDE.setFont(new Font("Times New Roman", Font.BOLD, 15));
		lbIDE.setBounds(212, 10, 79, 27);
		contentPane.add(lbIDE);
		
		 btnEliminar = new JButton("Eliminar");
		 btnEliminar.setIcon(new ImageIcon(VistaCliente.class.getResource("/resources/basura.png")));
		 btnEliminar.setEnabled(false);
		btnEliminar.setBounds(210, 248, 96, 27);
		contentPane.add(btnEliminar);
		
		btnReset = new JButton("Reset");
		btnReset.setIcon(new ImageIcon(VistaCliente.class.getResource("/resources/baseline_update_black_18dp.png")));
		btnReset.setBounds(116, 248, 85, 27);
		contentPane.add(btnReset);
		repaint();
		revalidate();
	}
}
