package Vista;

import javax.swing.JFrame;
import java.awt.BorderLayout;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JToolBar;

import Componentes.Tabla;

import javax.swing.JButton;
import javax.swing.JScrollPane;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Toolkit;
import javax.swing.ImageIcon;

public class VistaAplicacion extends JFrame {
	

	public VistaAplicacion() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(VistaAplicacion.class.getResource("/resources/gimnasio.png")));
		setLocation(550, 250);
		
		setTitle("JavaGym");
		
		setSize(600,550);
		getContentPane().setLayout(new BorderLayout(0, 0));
		setLocationRelativeTo(null);
		
	
		
		/*JPanel panelClientes = new JPanel();
		getContentPane().add(panelClientes, BorderLayout.CENTER);
		panelClientes.setLayout(new BorderLayout(0, 0));
		
		JToolBar toolBar = new JToolBar();
		panelClientes.add(toolBar, BorderLayout.NORTH);
		
		JButton btnNewButton = new JButton("Buscar");
		toolBar.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Nuevo");
		toolBar.add(btnNewButton_1);
		
		JScrollPane scrollPane = new JScrollPane();
		panelClientes.add(scrollPane, BorderLayout.CENTER);*/
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnNewMenu = new JMenu("Miembros");
		menuBar.add(mnNewMenu);
		
		JMenuItem mntmNewMenuItem = new JMenuItem("Clientes");
		mntmNewMenuItem.setIcon(new ImageIcon(VistaAplicacion.class.getResource("/resources/levantamiento-de-pesas.png")));
		mntmNewMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ClientePanel panel = new ClientePanel();
				
				
		getContentPane().removeAll();
		getContentPane().add(panel, BorderLayout.CENTER);
		repaint();
		revalidate();
			}
		});
		mnNewMenu.add(mntmNewMenuItem);
		
		JMenuItem mntmNewMenuItem_1 = new JMenuItem("Trabajadores");
		mntmNewMenuItem_1.setIcon(new ImageIcon(VistaAplicacion.class.getResource("/resources/fuerza.png")));
		mntmNewMenuItem_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			
			TrabajadorPanel panel2 = new TrabajadorPanel();
			getContentPane().removeAll();
			getContentPane().add(panel2,BorderLayout.CENTER);
			repaint();
			revalidate();
			}
		});
		mnNewMenu.add(mntmNewMenuItem_1);
		
		JMenu mnclases = new JMenu("Servicios");
		menuBar.add(mnclases);
		
		JMenuItem mntmNewMenuItem_2 = new JMenuItem("Clases");
		mntmNewMenuItem_2.setIcon(new ImageIcon(VistaAplicacion.class.getResource("/resources/gimnasio-1.png")));
		mntmNewMenuItem_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ClasePanel panel3 = new ClasePanel();
				getContentPane().removeAll();
				getContentPane().add(panel3,BorderLayout.CENTER);
				repaint();
				revalidate();
				}
				
			
		});
		mnclases.add(mntmNewMenuItem_2);
		
		JMenu mnNewMenu_3 = new JMenu("Registro");
		menuBar.add(mnNewMenu_3);
		
		JMenuItem mnClientela = new JMenuItem("Clientela");
		mnClientela.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
		registpanel regpanel=new registpanel();
				getContentPane().removeAll();
				getContentPane().add(regpanel,BorderLayout.CENTER);
				repaint();
				revalidate();
			}
		});
		mnClientela.setIcon(new ImageIcon(VistaAplicacion.class.getResource("/resources/registro.png")));
		mnNewMenu_3.add(mnClientela);
		
		JMenu mnNewMenu_2 = new JMenu("Configuracion");
		menuBar.add(mnNewMenu_2);
		
		JMenuItem mntmNewMenuItem_3 = new JMenuItem("Properties");
		mntmNewMenuItem_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new config();
			}
		});
		mntmNewMenuItem_3.setIcon(new ImageIcon(VistaAplicacion.class.getResource("/resources/documento.png")));
		mnNewMenu_2.add(mntmNewMenuItem_3);
		setVisible(true);
		repaint();
		revalidate();
	}
}
