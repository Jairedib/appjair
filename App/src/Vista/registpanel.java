package Vista;

import java.awt.BorderLayout;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JToolBar;

import Componentes.Tabla;

import java.awt.event.ActionListener;
import java.util.LinkedList;
import java.awt.event.ActionEvent;

public class registpanel extends JPanel{
	public JButton btnbuscar;
	public JButton btnactualizar;
	public Tabla tablaregis;
	
	public registpanel() {
		setVisible(true);
		revalidate();
		repaint();
		
		setLayout(new BorderLayout(0, 0));
		JToolBar toolBar = new JToolBar();
		this.add(toolBar, BorderLayout.NORTH);
		
		btnbuscar = new JButton("Buscar");
		btnbuscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				BusquedaRegistrado b = new BusquedaRegistrado();
				b.getRootPane().setDefaultButton(b.btnBuscar);
				b.btnBuscar.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						String idc= b.TFCLASE.getText().isEmpty()? null:b.TFCLASE.getText();
						Integer idcli= ((Integer)b.spIDcliente.getValue())==0? null:(Integer)b.spIDcliente.getValue();
						String f = b.TFFecha.getText().isEmpty()? null:b.TFFecha.getText();
						
						LinkedList<modelo.Registro> lista = modelo.Registro.find(idc, idcli, f);
						
						tablaregis.vaciar();
						for (modelo.Registro c:lista) {
							modelo.Cliente cli = modelo.Cliente.load(c.id_cliente);
							modelo.ClasesGym clas = modelo.ClasesGym.load(c.id_clase);
							Object [] fila = {c.id,clas.Nombre,cli.nombre,c.fecha_entrada,c.hora};
							tablaregis.modelo.addRow(fila);
						}
						
					}
				});
				
				
				
			}
		});
		btnbuscar.setIcon(new ImageIcon(registpanel.class.getResource("/resources/buscar.png")));
		toolBar.add(btnbuscar);
		
		btnactualizar= new JButton("");
		btnactualizar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				
			
				tablaregis.vaciar();
			
				for (modelo.Registro c:modelo.Registro.find(null, null, null)) {
					modelo.Cliente cli = modelo.Cliente.load(c.id_cliente);
					modelo.ClasesGym clas = modelo.ClasesGym.load(c.id_clase);
					Object[] fila= {c.id,clas.Nombre,cli.nombre,c.fecha_entrada,c.hora};
					tablaregis.modelo.addRow(fila);
				}
			}
		});
		
				
			
		btnactualizar.setIcon(new ImageIcon(registpanel.class.getResource("/resources/baseline_update_black_18dp.png")));
		toolBar.add(btnactualizar);
		JScrollPane scrollPane = new JScrollPane();
		this.add(scrollPane);
		
		Object[] cabecera= {"id","clase","cliente","fecha entrada","Hora"};
		Class<?>[] tipos = {Integer.class,String.class,String.class,String.class,String.class};
		Integer [] medidas= {80,100,120,90};
		 tablaregis = new Tabla(cabecera,tipos,medidas,null,null,null);
		scrollPane.setViewportView(tablaregis.tabla);
		tablaregis.vaciar();
		
	
	}
}
	

