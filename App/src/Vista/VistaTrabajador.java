package Vista;

import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;
import javax.swing.JSeparator;

public class VistaTrabajador  extends JFrame{
	public JTextField TFapellidos;
	public JTextField TFnombre;
	public JTextField TFtelefono;
	public JTextField TFdireccion;
	public JTextField TFnumsoc;
	public JTextField TFIban;
	public JTextField TFdni;
	public  JButton btGuardar;
	public  JButton btnReset;
	public  JButton btnEliminar;
	public JLabel lbide;
	public VistaTrabajador() {
		setVisible(true);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 342, 425);
		getContentPane().setLayout(null);
		setLocationRelativeTo(null);
		
		TFapellidos = new JTextField();
		TFapellidos.setHorizontalAlignment(SwingConstants.CENTER);
		TFapellidos.setColumns(10);
		TFapellidos.setBounds(162, 59, 144, 19);
		getContentPane().add(TFapellidos);
		
		JLabel lblNewLabel = new JLabel("Nombre");
		lblNewLabel.setFont(new Font("Times New Roman", Font.BOLD, 12));
		lblNewLabel.setBounds(10, 36, 45, 13);
		getContentPane().add(lblNewLabel);
		
		TFnombre = new JTextField();
		TFnombre.setHorizontalAlignment(SwingConstants.CENTER);
		TFnombre.setColumns(10);
		TFnombre.setBounds(10, 59, 142, 19);
		getContentPane().add(TFnombre);
		
		JLabel lblApellidos = new JLabel("Apellidos");
		lblApellidos.setFont(new Font("Times New Roman", Font.BOLD, 12));
		lblApellidos.setBounds(162, 36, 55, 13);
		getContentPane().add(lblApellidos);
		
		JLabel LBLtelefono = new JLabel("Telefono");
		LBLtelefono.setFont(new Font("Times New Roman", Font.BOLD, 12));
		LBLtelefono.setBounds(10, 105, 79, 13);
		getContentPane().add(LBLtelefono);
		
		TFtelefono = new JTextField();
		TFtelefono.setHorizontalAlignment(SwingConstants.CENTER);
		TFtelefono.setColumns(10);
		TFtelefono.setBounds(10, 127, 142, 19);
		getContentPane().add(TFtelefono);
		
		JLabel lblDireccion = new JLabel("Direccion");
		lblDireccion.setFont(new Font("Times New Roman", Font.BOLD, 12));
		lblDireccion.setBounds(162, 105, 55, 13);
		getContentPane().add(lblDireccion);
		
		TFdireccion = new JTextField();
		TFdireccion.setHorizontalAlignment(SwingConstants.CENTER);
		TFdireccion.setColumns(10);
		TFdireccion.setBounds(162, 127, 144, 19);
		getContentPane().add(TFdireccion);
		
		JLabel lblNumSeguridadSocial = new JLabel("Num seguridad social");
		lblNumSeguridadSocial.setFont(new Font("Times New Roman", Font.BOLD, 12));
		lblNumSeguridadSocial.setBounds(10, 168, 142, 13);
		getContentPane().add(lblNumSeguridadSocial);
		
		TFnumsoc = new JTextField();
		TFnumsoc.setHorizontalAlignment(SwingConstants.CENTER);
		TFnumsoc.setColumns(10);
		TFnumsoc.setBounds(10, 191, 296, 19);
		getContentPane().add(TFnumsoc);
		
		btGuardar = new JButton("Guardar");
		btGuardar.setIcon(new ImageIcon(VistaTrabajador.class.getResource("/resources/salvar.png")));
		btGuardar.setBounds(9, 349, 96, 27);
		getContentPane().add(btGuardar);
		
		btnReset = new JButton("Reset");
		btnReset.setIcon(new ImageIcon(VistaTrabajador.class.getResource("/resources/baseline_update_black_18dp.png")));
		btnReset.setBounds(115, 349, 85, 27);
		getContentPane().add(btnReset);
		
		btnEliminar = new JButton("Eliminar");
		btnEliminar.setIcon(new ImageIcon(VistaTrabajador.class.getResource("/resources/basura.png")));
		btnEliminar.setEnabled(false);
		btnEliminar.setBounds(210, 349, 96, 27);
		getContentPane().add(btnEliminar);
		
		TFIban = new JTextField();
		TFIban.setHorizontalAlignment(SwingConstants.CENTER);
		TFIban.setColumns(10);
		TFIban.setBounds(10, 240, 296, 19);
		getContentPane().add(TFIban);
		
		JLabel lblIban = new JLabel("IBAN");
		lblIban.setFont(new Font("Times New Roman", Font.BOLD, 12));
		lblIban.setBounds(10, 220, 45, 13);
		getContentPane().add(lblIban);
		
		TFdni = new JTextField();
		TFdni.setHorizontalAlignment(SwingConstants.CENTER);
		TFdni.setColumns(10);
		TFdni.setBounds(10, 293, 296, 19);
		getContentPane().add(TFdni);
		
		JLabel lblDni = new JLabel("DNI");
		lblDni.setFont(new Font("Times New Roman", Font.BOLD, 12));
		lblDni.setBounds(10, 270, 45, 13);
		getContentPane().add(lblDni);
		
		lbide = new JLabel("");
		lbide.setBounds(243, 10, 45, 13);
		getContentPane().add(lbide);
		repaint();
		revalidate();
		
	}
}
