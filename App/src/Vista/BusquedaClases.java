package Vista;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JSpinner;
import javax.swing.ImageIcon;

public class BusquedaClases extends JFrame {
	public JTextField tfNombre;
	public JTextField tffechaini;
	public JTextField tffechafin;
	 public JButton btnBuscar;
	 public JComboBox<modelo.Trabajador> cbProfe;
	 public JSpinner spID;
	 
	
	
	public BusquedaClases() {
		getContentPane().setLayout(null);
		setVisible(true);
		revalidate();
		repaint();
		setSize(300,300);
		setLocationRelativeTo(null);
		
		tfNombre = new JTextField();
		tfNombre.setBounds(149, 48, 116, 19);
		getContentPane().add(tfNombre);
		tfNombre.setColumns(10);
		
		cbProfe = new JComboBox<modelo.Trabajador>();
		cbProfe.setBounds(10, 47, 116, 21);
		getContentPane().add(cbProfe);
		
		tffechaini = new JTextField();
		tffechaini.setBounds(10, 113, 116, 19);
		getContentPane().add(tffechaini);
		tffechaini.setColumns(10);
		
		tffechafin = new JTextField();
		tffechafin.setBounds(149, 113, 116, 19);
		getContentPane().add(tffechafin);
		tffechafin.setColumns(10);
		
		 btnBuscar = new JButton("Buscar");
		 btnBuscar.setIcon(new ImageIcon(BusquedaClases.class.getResource("/resources/buscar.png")));
		btnBuscar.setBounds(108, 166, 116, 21);
		getContentPane().add(btnBuscar);
		
		JLabel lblNewLabel = new JLabel("Profesor");
		lblNewLabel.setBounds(10, 25, 45, 13);
		getContentPane().add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Clase");
		lblNewLabel_1.setBounds(149, 25, 45, 13);
		getContentPane().add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("Fecha de Inicio");
		lblNewLabel_2.setBounds(10, 94, 72, 13);
		getContentPane().add(lblNewLabel_2);
		
		JLabel lblNewLabel_3 = new JLabel("hora");
		lblNewLabel_3.setBounds(149, 94, 75, 13);
		getContentPane().add(lblNewLabel_3);
		
		spID = new JSpinner();
		spID.setBounds(26, 167, 45, 20);
		getContentPane().add(spID);
	}
}



