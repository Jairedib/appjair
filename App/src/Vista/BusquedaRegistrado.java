package Vista;

import javax.swing.JFrame;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.ImageIcon;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class BusquedaRegistrado extends JFrame {
	public JTextField TFFecha;
	public 	JButton btnBuscar;
	public JSpinner spIDcliente;
	public JTextField TFCLASE;
	
	public BusquedaRegistrado() {
		getContentPane().setLayout(null);
		repaint();
		revalidate();
		setVisible(true);
		setSize(388,186);
		setLocationRelativeTo(null);
		
		
		spIDcliente = new JSpinner();
		spIDcliente.setBounds(44, 35, 54, 20);
		getContentPane().add(spIDcliente);
		
		TFFecha = new JTextField();
		TFFecha.setBounds(248, 35, 96, 19);
		getContentPane().add(TFFecha);
		TFFecha.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("ID CLIENTE");
		lblNewLabel.setBounds(40, 12, 58, 13);
		getContentPane().add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("CLASE");
		lblNewLabel_1.setBounds(131, 12, 45, 13);
		getContentPane().add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("FECHA");
		lblNewLabel_2.setBounds(248, 12, 45, 13);
		getContentPane().add(lblNewLabel_2);
		
		 btnBuscar = new JButton("Buscar");
		btnBuscar.setIcon(new ImageIcon(BusquedaRegistrado.class.getResource("/resources/buscar.png")));
		btnBuscar.setBounds(142, 102, 85, 21);
		getContentPane().add(btnBuscar);
		
		TFCLASE = new JTextField();
		TFCLASE.setBounds(131, 35, 96, 19);
		getContentPane().add(TFCLASE);
		TFCLASE.setColumns(10);
	}
}
