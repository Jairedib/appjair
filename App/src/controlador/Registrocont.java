
	package controlador;
	import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.LinkedList;

import Componentes.Tabla;

import java.awt.event.ActionEvent;

	public class Registrocont {
		
		public Tabla tablaclientes;
		public Vista.Vistaregistrado Vregis;
		public modelo.Registro regis;
		public modelo.ClasesGym clas;
		
	
		
		public Registrocont(Integer i) {
			this();
		
			clas=modelo.ClasesGym.load(i);
			
			if(clas!=null) {
			
			Vregis.tfclase.setText(clas.id+"");
				//rellenaprofesFromDatabase();
			//Vregis.tfcliente.setText(regis.id_cliente+"");
			Vregis.tffechaini.setText(clas.fecha_inicio);
			Vregis.tffechafin.setText(clas.hora);
			Vregis.repaint();
			Vregis.revalidate();
			/*for (int i1 =0;i1<Vregis.cbclase.getItemCount();i1++) {
				modelo.ClasesGym mod = (modelo.ClasesGym)Vregis.cbclase.getItemAt(i1);
				if (mod.id==regis.)  {
					Vregis.cbclase.setSelectedIndex(i1);
					Vregis.repaint();
					Vregis.revalidate();
					
				}
				
				
			}*/
	
			}
			
		}
		
		public Registrocont() {
		
			Vregis = new Vista.Vistaregistrado();
			//rellenaprofesFromDatabase();
			cargarclientes();
			
			Vregis.btnGuardar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					guardar();
				
	}
				
			});
			
			 Vregis.btnReset.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						reset();
					}
				});
		}
			
				private void guardar() {
				
					
				regis = modelo.Registro.create(	
				Integer.parseInt(Vregis.tfclase.getText()),
												Integer.parseInt(Vregis.tfcliente.getText()),
												Vregis.tffechaini.getText(),
												Vregis.tffechafin.getText());
				//((modelo.ClasesGym)Vregis.cbclase.getSelectedItem()).id,
				
			if (regis!=null) {
			
				varios.Datos.minfo("Clase Guardada");
				
				
			}	
			
			
			}
			
			
			
		private void reset() {

			
				Vregis.tfcliente.setText("");
				regis=null;
				
				
			}
		
		public void cargarclientes() {
			
			Object[] cabecera= {"id","nombre","apellidos","telefono","direccion","email"};
			Class<?>[] tipos = {Integer.class,String.class,String.class,String.class,String.class,String.class};
			Integer [] medidas= {80,100,120,90,130,130};
			tablaclientes = new Tabla(cabecera,tipos,medidas,null,null,null);

			Vregis.scrollPane.setViewportView(tablaclientes.tabla);
			tablaclientes.tabla.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
					if(e.getClickCount()==2 && e.getButton()==1) {
						Integer id=(int) tablaclientes.getValueSelected(0);
					Vregis.tfcliente.setText(id+"");
					Vregis.repaint();
					Vregis.revalidate();
				
					}
				}
			});
			LinkedList<modelo.Cliente> lista = modelo.Cliente.find(null,null,null,null);
			tablaclientes.vaciar();
			for (modelo.Cliente c:lista) {
				Object[] fila= {c.ID,c.nombre,c.apellidos,c.telefono,c.direccion,c.email};
				tablaclientes.modelo.addRow(fila);
				
			
			}
			Vregis.repaint();
			Vregis.revalidate();
			
			
		}
		/*private void rellenaprofesFromDatabase() {
			
			LinkedList<modelo.ClasesGym> l=modelo.ClasesGym.find(null,null,null,null,null);
			Vregis.cbclase.removeAllItems();
			Vregis.cbclase.addItem(new modelo.ClasesGym(null,null,null, null, null));
			for (modelo.ClasesGym ci:l) {
				Vregis.cbclase.addItem(ci);
				
				}
			}
		*/

			
		
			
		}
	


