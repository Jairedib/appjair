package controlador;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import varios.Datos;

import java.awt.event.ActionEvent;

public class Clientecont {
		
			
		public Vista.VistaCliente VCliente;
		public modelo.Cliente cli;
		
		public Clientecont(Integer i) {
			this();
		
			cli=modelo.Cliente.load(i);
			
			if(cli!=null) {
			
			VCliente.TFNombre.setText(cli.nombre);
			VCliente.TFApellidos.setText(cli.apellidos);
			VCliente.TFTelefono.setText(cli.telefono);
			VCliente.TFDireccion.setText(cli.direccion);
			VCliente.TFPoblacion.setText(cli.dni);
			VCliente.TFEmail.setText(cli.email);
			VCliente.lbIDE.setText(cli.ID+"");
			VCliente.btnEliminar.setEnabled(true);
			
			}
			
		}
		
		public Clientecont() {
			VCliente = new Vista.VistaCliente();
			
			VCliente.btGuardar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					guardar();
				
	}
				
			});
			
			 VCliente.btnEliminar.addActionListener(new ActionListener() {
				 	public void actionPerformed(ActionEvent e) {
						if (Datos.mConfirmacion("�Est�s Seguro?")==JOptionPane.YES_OPTION) 
				 		eliminar();
				 
				 	
				 	}
				 });
			 
			 VCliente.btnReset.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						reset();
					}
				});
		}
			
				private void guardar() {
					
				if(cli==null) {
					
				cli = modelo.Cliente.create(VCliente.TFNombre.getText(),
											VCliente.TFApellidos.getText(),
											VCliente.TFTelefono.getText(),
											VCliente.TFDireccion.getText(),
											VCliente.TFPoblacion.getText(),
											VCliente.TFEmail.getText());
				
				
			if (cli!=null) {
				VCliente.lbIDE.setText(cli.ID+"");
				VCliente.btnEliminar.setEnabled(true);
				varios.Datos.minfo("Cliente Guardado");
				
				
			}	
			} else {
				cli.nombre=VCliente.TFNombre.getText();
				cli.apellidos=VCliente.TFApellidos.getText();
				cli.telefono=VCliente.TFTelefono.getText();
				cli.direccion=VCliente.TFDireccion.getText();
				cli.dni=VCliente.TFPoblacion.getText();
				cli.email=VCliente.TFEmail.getText();
				cli.update();
				varios.Datos.minfo("Cliente Actualizado");
				
			}
		
			
				}
				
		
			
		private void eliminar() {
				int err=cli.delete();
				if(err==0) {
					reset();
				varios.Datos.minfo("Cliente eliminado");
				}
				
			}
			
		private void reset() {

				VCliente.TFNombre.setText("");
				VCliente.TFApellidos.setText("");
				VCliente.TFTelefono.setText("");
				VCliente.TFDireccion.setText("");
				VCliente.TFPoblacion.setText("");
				VCliente.TFEmail.setText("");
				VCliente.lbIDE.setText("");
				VCliente.btnEliminar.setEnabled(false);
				cli=null;
				
			}

			
		
			
		}
	


