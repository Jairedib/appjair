package controlador;
import java.awt.event.ActionListener;
import java.util.LinkedList;

import javax.swing.JOptionPane;

import Vista.VistaClases;
import modelo.ClasesGym;

import modelo.Trabajador;
import varios.Datos;

import java.awt.event.ActionEvent;

public class Clasescont {
		
			
		public Vista.VistaClases VClases;
		public modelo.ClasesGym clas;
		public Vista.ClasePanel b;
		
		public Clasescont(Integer i) {
			this();
		
			clas=modelo.ClasesGym.load(i);
			rellenaprofesFromDatabase();
			
			if(clas!=null) {
			
			VClases.tfNombre.setText(clas.Nombre);
			VClases.tffechaini.setText(clas.fecha_inicio);
			VClases.tffechafin.setText(clas.hora);
			rellenaprofesFromDatabase();
			VClases.btnEliminar.setEnabled(true);
			for (int i1 =0;i1<VClases.cbProfe.getItemCount();i1++) {
				modelo.Trabajador mod = (modelo.Trabajador)VClases.cbProfe.getItemAt(i1);
				if (mod.id==clas.idTrabajadores)  {
					VClases.cbProfe.setSelectedIndex(i1);
					VClases.repaint();
					VClases.revalidate();
					
				}
				
				
			}
			}
			
		}
		
		public Clasescont() {
			
			VClases = new Vista.VistaClases();
			rellenaprofesFromDatabase();
			
			VClases.btnGuardar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					guardar();
			
				}
	
		});
				
			 VClases.btnEliminar.addActionListener(new ActionListener() {
				 	public void actionPerformed(ActionEvent e) {
				 		if (Datos.mConfirmacion("�Est�s Seguro?")==JOptionPane.YES_OPTION) 
				 			eliminar();
				
				 	}
				 });
			 
			 VClases.btnReset.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						reset();
					}
				});
		}
			
				private void guardar() {
					
				if(clas==null) {
					
				clas = modelo.ClasesGym.create(VClases.tfNombre.getText(),
											((modelo.Trabajador)VClases.cbProfe.getSelectedItem()).id,
											VClases.tffechaini.getText(),
											VClases.tffechafin.getText());
				
				
			if (clas!=null) {
				VClases.btnEliminar.setEnabled(true);
				varios.Datos.minfo("Clase Guardada");
				
				
			}	
			} else {
				clas.Nombre=VClases.tfNombre.getText();
				clas.fecha_inicio=VClases.tffechaini.getText();
				clas.hora=VClases.tffechafin.getText();
				clas.idTrabajadores=((modelo.Trabajador)VClases.cbProfe.getSelectedItem()).id;
				clas.update();
				varios.Datos.minfo("Clase Actualizada");
				
			}
				
		
				}
				
				private void rellenaprofesFromDatabase() {
					
						LinkedList<modelo.Trabajador> l=modelo.Trabajador.find(null,null,null,null);
						VClases.cbProfe.removeAllItems();
						//b.b.cbProfe.addItem(new modelo.Trabajador(null,"Selecciona",null,null, null, null, null, null));
						
						for (modelo.Trabajador ci:l) {
							VClases.cbProfe.addItem(ci);
							
							}
						}
				
				
			
		private void eliminar() {
				int err=clas.delete();
				if(err==0) {
					reset();
					
					
				varios.Datos.minfo("Clase eliminada");
				}
				
			}
			
		private void reset() {

				VClases.tfNombre.setText("");
				VClases.tffechaini.setText("");
				VClases.tffechafin.setText("");
				//VClases.cbProfe.setText("");
				VClases.btnEliminar.setEnabled(false);
				clas=null;
				
			}

			
		
			
		}
	


