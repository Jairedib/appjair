package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import varios.Datos;

public class Trabajadorcont {
	
	public Vista.VistaTrabajador vtrabajador;
	public modelo.Trabajador trab;
	
	public Trabajadorcont(Integer i) {
		this();
	
		trab=modelo.Trabajador.load(i);
	
		if(trab!=null) {
	
		vtrabajador.TFnombre.setText(trab.nombre);
		vtrabajador.TFapellidos.setText(trab.apellidos);
		vtrabajador.TFtelefono.setText(trab.telefono);
		vtrabajador.TFnumsoc.setText(trab.num_seguridad_social);
		vtrabajador.TFdireccion.setText(trab.direccion);
		vtrabajador.TFIban.setText(trab.Cuenta_banco);
		vtrabajador.TFdni.setText(trab.dni);
		vtrabajador.lbide.setText(trab.id+"");
		vtrabajador.btnEliminar.setEnabled(true);
		
		}
		
	}
	
	public Trabajadorcont() {
		vtrabajador = new Vista.VistaTrabajador();
		
		vtrabajador.btGuardar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				guardar();
			
}
			
		});
		
		 vtrabajador.btnEliminar.addActionListener(new ActionListener() {
			 	public void actionPerformed(ActionEvent e) {
					if (Datos.mConfirmacion("�Est�s Seguro?")==JOptionPane.YES_OPTION) 
			 		eliminar();
			 
			 	
			 	}
			 });
		 
		 vtrabajador.btnReset.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					reset();
				}
			});
	}
		
			private void guardar() {
				
			if(trab==null) {
				
			trab = modelo.Trabajador.Create(vtrabajador.TFnombre.getText(),
											vtrabajador.TFapellidos.getText(),
											vtrabajador.TFtelefono.getText(),					
											vtrabajador.TFnumsoc.getText(),
											vtrabajador.TFdireccion.getText(),
											vtrabajador.TFIban.getText(),
											vtrabajador.TFdni.getText());
			
			
		if (trab!=null) {
			vtrabajador.lbide.setText(trab.id+"");
			vtrabajador.btnEliminar.setEnabled(true);
			varios.Datos.minfo("Trabajador  Guardado");
			
			
		}	
		} else {
			trab.nombre=vtrabajador.TFnombre.getText();
			trab.apellidos=vtrabajador.TFapellidos.getText();		
			trab.telefono=vtrabajador.TFtelefono.getText();
			trab.direccion=vtrabajador.TFdireccion.getText();
			trab.num_seguridad_social=vtrabajador.TFnumsoc.getText();
			trab.Cuenta_banco=vtrabajador.TFIban.getText();
			trab.dni=vtrabajador.TFdni.getText();
			trab.update();
			varios.Datos.minfo("Trabajador Actualizado");
			
		}
	
			
	
		
			
			}
			
	
		
	private void eliminar() {
			int err=trab.delete();
			if(err==0) {
				reset();
			varios.Datos.minfo("Trabjador eliminado");
			}
			
		}
		
	private void reset() {

			vtrabajador.TFnombre.setText("");
			vtrabajador.TFapellidos.setText("");
			vtrabajador.TFtelefono.setText("");
			vtrabajador.TFnumsoc.setText("");
			vtrabajador.TFdireccion.setText("");
			vtrabajador.TFIban.setText("");
			vtrabajador.TFdni.setText("");
			vtrabajador.btnEliminar.setEnabled(false);
			trab=null;
			
		}

		
	
		
	}




