package modelo;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;

import varios.Datos;
import varios.Fecha;

public class ClasesGym {
	
	public Integer id;
	public String Nombre;
	public Integer idTrabajadores;
	public String fecha_inicio;
	public String hora;
	
	public ClasesGym (Integer id,Integer idtrab, String n,  String ini,String fin) {
		this.id = id;
		this.idTrabajadores = idtrab;
		this.fecha_inicio = ini;
		this.hora = fin;
		this.Nombre=n;
		
	}
	public int delete(){
		return ClasesGym.delete(this.id);
		
	}
	
	public static int delete(int i) {
		Conexion.open();
		PreparedStatement stm;
		try {
			stm= Conexion.con.prepareStatement("Delete from clases  where id=? ");
			// cambiamos el interrogante por el valor que nos pasan, i;
			stm.setInt(1, i);
			//ejecutamos executeupdate para ejecutar los datos
			stm.executeUpdate();
			stm.close();
			return 0;
			
			// sale informacion del error solamente para cuando el programa este en produccion, no en desarrollo
		} catch (SQLException e) {
			if (Datos.debug) e.printStackTrace();
			Datos.mError(e.getErrorCode()+" - "+e.getLocalizedMessage());
			return e.getErrorCode();
		}
		 
		
		}
	public static ClasesGym create ( String n,Integer idtrab, String ini,String fin) {
		
		Conexion.open();
		PreparedStatement insercion;
		// El insert me devuelve una respuesta, que se guardara en resultSet
		ResultSet respuesta;
		try {
			
			insercion=Conexion.con.prepareStatement("insert into clases (idtrabajadores,nombre, fecha_clase_inicio,hora) values (?,?,?,?)", Statement.RETURN_GENERATED_KEYS);	
			insercion.setInt(1, idtrab);
			insercion.setString(2, n);
			insercion.setString(3,ini);
			insercion.setString(4,fin);
			
			insercion.executeUpdate();
			respuesta = insercion.getGeneratedKeys();
			
	
			
			if(respuesta.next()) return ClasesGym.load(respuesta.getInt(1));
			insercion.close();
		} catch (SQLException e) {
			if (Datos.debug) e.printStackTrace();
			Datos.mError(e.getErrorCode()+" - "+e.getLocalizedMessage());
	
			
		
		}
		return null;
		
		
		
	}
		public static ClasesGym load(Integer i) {
			Conexion.open();
			PreparedStatement stm;
			ResultSet respuesta;
			try {
				stm = Conexion.con.prepareStatement("select * from clases where id=?");
				stm.setInt(1, i);
				respuesta = stm.executeQuery();
				if(respuesta.next()) {
					return new ClasesGym(respuesta.getInt("id"),
										 respuesta.getInt("idtrabajadores"),
										 respuesta.getString("nombre"),
										 respuesta.getString("fecha_clase_inicio"), 
										 respuesta.getString("hora"));
							
				}
				stm.close();
			} catch (SQLException e) {
				if (Datos.debug) e.printStackTrace();
				Datos.mError(e.getErrorCode()+" - "+e.getLocalizedMessage());
			}
			return null;
		}
		public void update() {
			Conexion.open();
			PreparedStatement stm;
			ResultSet respuesta;
			try {
				stm=Conexion.con.prepareStatement("update clases  set  idtrabajadores=?,nombre=?,fecha_clase_inicio=?,hora=? where id=? ");
				stm.setInt(1,this.idTrabajadores);
				stm.setString(2,this.Nombre);
				stm.setString(3,this.fecha_inicio);
				stm.setString(4,this.hora);
				stm.setInt(5,this.id);
				stm.executeUpdate();
				stm.close();
				
			} catch (SQLException e) {
				if (Datos.debug) e.printStackTrace();
				Datos.mError(+e.getErrorCode()+" - "+e.getLocalizedMessage());
			}
		}
		
		public static LinkedList<ClasesGym> find ( Integer id,Integer idtrab, String n,String ini,String fin){
			Conexion.open();
			PreparedStatement stm;
			ResultSet respuesta;
			
			LinkedList<ClasesGym> lista = new LinkedList<ClasesGym>();
			int x = 1;
			String consulta = "Select * from clases where ";
			if(id!=null) consulta+= "id =? and ";
			if(idtrab!=null) consulta+= "idtrabajadores =? and ";
			if(n!=null) consulta+= "nombre like? and ";
			if(ini!=null) consulta+= "Fecha_clase_inicio like? and ";
			if(fin!=null) consulta+= "hora like? and ";
			consulta+=1+1;
			
			try {
				stm = Conexion.con.prepareStatement(consulta);
				if(id!=null) {stm.setInt(x,id);x++;}
				if(idtrab!=null) {stm.setInt(x,idtrab);x++;}
				if(n!=null) {stm.setString(x,n+"%");x++;}
				if(ini!=null) {stm.setString(x, ini+"%");x++;}
				if(fin!=null) {stm.setString(x, fin+"%");x++;}
				
				respuesta = stm.executeQuery();
				while (respuesta.next()) {
					lista.add(new ClasesGym (respuesta.getInt("id"), 
											 respuesta.getInt("idtrabajadores"),
											 respuesta.getString("nombre"),
											 respuesta.getString("fecha_clase_inicio"), 
											 respuesta.getString("hora"))); 
				}
				stm.close();
				
			} catch (SQLException e) {
				if (Datos.debug) e.printStackTrace();
				Datos.mError(e.getErrorCode()+" - "+e.getLocalizedMessage());
			}
			return lista;
			
		}
		
		
			
		}
	

		
		
	
	
	
	
	


