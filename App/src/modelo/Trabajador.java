package modelo;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;

import varios.Datos;

public class Trabajador {
	
	public Integer id;
	public String nombre;
	public String apellidos;
	public String telefono;
	public String direccion;
	public String num_seguridad_social;
	public String dni;
	public String Cuenta_banco;
	
	public Trabajador(Integer id,String n,String ap, String dir,String tel,String numsoc,String banc,String dni) {
		
		this.id=id;
		this.nombre=n;
		this.apellidos=ap;
		this.telefono=tel;
		this.direccion=dir;
		this.num_seguridad_social=numsoc;
		this.dni=dni;
		this.Cuenta_banco=banc;
		
	}
	public int delete(){
		 return Trabajador.delete(this.id);
		
		
	}
	
	public static int delete(int i) {
		Conexion.open();
		PreparedStatement stm;
		try {
			stm = Conexion.con.prepareStatement("delete trabajadores from trabajadores where idtrabajadores=?");
			stm.setInt(1, i);
			stm.executeUpdate();
			stm.close();
			return 0;
			
		} catch (SQLException e) {
			if (Datos.debug) e.printStackTrace();
			Datos.mError(+e.getErrorCode()+" - "+e.getLocalizedMessage());
			return e.getErrorCode();
		}
		
	}
	
	public static Trabajador Create(String n,String ap, String dir,String tel,String numsoc,String dni, String banc) {
		Conexion.open();
		PreparedStatement insercion;
		ResultSet respuesta;
		try {
			insercion = Conexion.con.prepareStatement("Insert into trabajadores(nombre,apellidos,telefono,num_seguridad_social,direccion,Cuenta_banco,dni) values (?,?,?,?,?,?,?)", Statement.RETURN_GENERATED_KEYS);
			insercion.setString(1,n);
			insercion.setString(2, ap);
			insercion.setString(3, tel);
			insercion.setString(4, numsoc);
			insercion.setString(5, dir);
			insercion.setString(6, banc);
			insercion.setString(7, dni);
			insercion.executeUpdate();
			respuesta=insercion.getGeneratedKeys();
			if(respuesta.next())return Trabajador.load(respuesta.getInt(1));
			
		
		} catch (SQLException e) {
			if (Datos.debug) e.printStackTrace();
			Datos.mError(+e.getErrorCode()+" - "+e.getLocalizedMessage());
			
			
		}
		return null;
		
		
	}
	
	public static Trabajador load(Integer i) {
		Conexion.open();
		PreparedStatement stm;
		ResultSet respuesta;
		
		try {
			stm = Conexion.con.prepareStatement("Select * from trabajadores where idtrabajadores=?");
			stm.setInt(1,i);
			respuesta = stm.executeQuery();
		
			if(respuesta.next()) {
				return new Trabajador(respuesta.getInt("idtrabajadores"),
									  respuesta.getString("nombre"),
									  respuesta.getString("apellidos"),
									  respuesta.getString("telefono"),
									  respuesta.getString("num_seguridad_social"),
									  respuesta.getString("direccion"),
									  respuesta.getString("Cuenta_banco"),
									  respuesta.getString("dni"));			  
					
				
				
			}
			stm.close();
			
		} catch (SQLException e) {
			if (Datos.debug) e.printStackTrace();
			Datos.mError(+e.getErrorCode()+" - "+e.getLocalizedMessage());
			
		}
		return null;
		
	}
	
	public void update() {
		Conexion.open();
		PreparedStatement stm;
		ResultSet respuesta;
		try {
			stm=Conexion.con.prepareStatement("update trabajadores  set  nombre=?,apellidos=?,telefono=?,direccion=?,num_seguridad_social=?,Cuenta_banco=?,dni=? where idtrabajadores=? ");
			stm.setString(1,this.nombre);
			stm.setString(2,this.apellidos);
			stm.setString(3,this.telefono);
			stm.setString(4,this.num_seguridad_social);
			stm.setString(5,this.direccion);
			stm.setString(6, this.Cuenta_banco);
			stm.setString(7, this.dni);
			stm.setInt(8,this.id);
			stm.executeUpdate();
			stm.close();
			
		} catch (SQLException e) {
			if (Datos.debug) e.printStackTrace();
			Datos.mError(+e.getErrorCode()+" - "+e.getLocalizedMessage());
		}
	}
	
	public static LinkedList<Trabajador>find(Integer i,String n,String ap,String dni){
	
		Conexion.open();
		PreparedStatement stm;
		ResultSet respuesta;
		int x=1;
		LinkedList<Trabajador> lista= new LinkedList<Trabajador>();
		String consulta="Select * from trabajadores where ";
		if(i!=null)  consulta+=" idtrabajadores=? and ";
		if(n!=null)  consulta+=" nombre like ? and ";
		if(ap!=null) consulta+=" apellidos like ? and ";
		if(dni!=null)  consulta+=" dni like ? and ";
		consulta+="1+1";
		try {
			stm=Conexion.con.prepareStatement(consulta);
			if(i!=null)  {stm.setInt(x, i);x++;}
			if(n!=null)  {stm.setString(x, n+"%");x++;}
			if(ap!=null) {stm.setString(x, ap+"%");x++;}
			if(dni!=null)  {stm.setString(x, dni+"%");x++;}
			
			respuesta=stm.executeQuery();
			while(respuesta.next()) {
				lista.add(new Trabajador(respuesta.getInt("idtrabajadores"),
								   respuesta.getString("nombre"),
								   respuesta.getString("apellidos"),
								   respuesta.getString("telefono"),
								   respuesta.getString("num_seguridad_social"),								 
								   respuesta.getString("direccion"),
								   respuesta.getString("Cuenta_banco"),
								   respuesta.getString("dni")));
			
			}
			stm.close();
		} catch (SQLException e) {
			if (Datos.debug) e.printStackTrace();
			Datos.mError(+e.getErrorCode()+" - "+e.getLocalizedMessage());
		}		
			return lista;
		
		
	}
	public String toString() {
		return this.nombre;
	}

}
