package modelo;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;

import javax.swing.JOptionPane;

import varios.Datos;

public class Cliente {

	public String nombre;
	public String apellidos;
	public String direccion;
	public String CP;
	public String telefono;
	public Integer ID;
	public String email;
	public String dni;
	
	private Cliente(Integer id,String n,String ap,String t,String dir, String dni, String em){
		this.ID=id;
		this.nombre=n;
		this.apellidos=ap;
		this.telefono=t;
		this.direccion=dir;
		this.dni=dni;
		this.email=em;
	}
	public int delete() {
		return Cliente.delete(this.ID);
	}
	public static int delete(int i) {
		Conexion.open();
		PreparedStatement stm;
		try {
			stm=Conexion.con.prepareStatement("delete clientes from clientes where idclientes=?");
			stm.setInt(1, i);
			stm.executeUpdate();
			stm.close();
			return 0;
		} catch (SQLException e) {
			if (Datos.debug) e.printStackTrace();
			Datos.mError(+e.getErrorCode()+" - "+e.getLocalizedMessage());
			return e.getErrorCode();
		}
		
	}
	public static Cliente create (String n,String ap,String t,String dir, String dni,String em) {
		Conexion.open();
		PreparedStatement insercion;
		ResultSet respuesta;
		try {
			insercion=Conexion.con.prepareStatement("insert into Clientes(nombre,apellido,telefono,direccion,dni,email) values (?,?,?,?,?,?) ", Statement.RETURN_GENERATED_KEYS);
			insercion.setString(1,n);
			insercion.setString(2,ap);
			insercion.setString(3,t);
			insercion.setString(4,dir);
			insercion.setString(5,dni);
			insercion.setString(6, em);
			insercion.executeUpdate();
			respuesta=insercion.getGeneratedKeys();
		
			
			//Integer ID=respuesta.getInt(1);
			/* recogeremos el id y cargaremos el cliente */
			if (respuesta.next()) return Cliente.load(respuesta.getInt(1));
			

		} catch (SQLException e) {
			if (Datos.debug) e.printStackTrace();
			Datos.mError(+e.getErrorCode()+" - "+e.getLocalizedMessage());
			
		}
		

		return null;
	}
	
	public static Cliente load(Integer i) {
		Conexion.open();
		PreparedStatement stm;
		ResultSet respuesta;
	
		
		try {
			stm = Conexion.con.prepareStatement("select * from clientes where idclientes=?" );
			stm.setInt(1,i);
			respuesta=stm.executeQuery();
			if (respuesta.next()) {
				return new Cliente(respuesta.getInt("idclientes"),
								   respuesta.getString("nombre"),
								   respuesta.getString("apellido"),
								   respuesta.getString("direccion"),								 
								   respuesta.getString("telefono"),
								   respuesta.getString("dni"),
								   respuesta.getString("email"));
				
			} 
			
			stm.close();
			
		} catch (SQLException e) {
			if (Datos.debug) e.printStackTrace();
			Datos.mError(+e.getErrorCode()+" - "+e.getLocalizedMessage());
		}
		
		return null;
		
	}
	public void update() {
		Conexion.open();
		PreparedStatement stm;
		ResultSet respuesta;
		try {
			stm=Conexion.con.prepareStatement("update clientes  set  nombre=?,apellido=?,telefono=?,direccion=?,dni=?,email=? where idclientes=? ");
			stm.setString(1,this.nombre);
			stm.setString(2,this.apellidos);
			stm.setString(3,this.direccion);
			stm.setString(4,this.telefono);
			stm.setString(5,this.dni);
			stm.setString(6, this.email);
			stm.setInt(7,this.ID);
			stm.executeUpdate();
			stm.close();
			
		} catch (SQLException e) {
			if (Datos.debug) e.printStackTrace();
			Datos.mError(+e.getErrorCode()+" - "+e.getLocalizedMessage());
		}
	}
	
	public static LinkedList<Cliente> find (Integer i,String n,String ap,String dni){
		Conexion.open();
		PreparedStatement stm;
		ResultSet respuesta;
		int x=1;
		LinkedList<Cliente> lista= new LinkedList<Cliente>();
		String consulta="Select * from clientes where ";
		if(i!=null)  consulta+=" idclientes=? and ";
		if(n!=null)  consulta+=" nombre like ? and ";
		if(ap!=null) consulta+=" apellido like ? and ";
		if(dni!=null)  consulta+=" dni like ? and ";
		consulta+="1+1";
		
		try {
			stm=Conexion.con.prepareStatement(consulta);
			if(i!=null)  {stm.setInt(x, i);x++;}
			if(n!=null)  {stm.setString(x, n+"%");x++;}
			if(ap!=null) {stm.setString(x, ap+"%");x++;}
			if(dni!=null)  {stm.setString(x, dni+"%");x++;}
			
			respuesta=stm.executeQuery();
			while(respuesta.next()) {
				lista.add(new Cliente(respuesta.getInt("idclientes"),
								   respuesta.getString("nombre"),
								   respuesta.getString("apellido"),
								   respuesta.getString("direccion"),								 
								   respuesta.getString("telefono"),
								   respuesta.getString("dni"),
								   respuesta.getString("email")));
			}
			stm.close();
		} catch (SQLException e) {
			if (Datos.debug) e.printStackTrace();
			Datos.mError(+e.getErrorCode()+" - "+e.getLocalizedMessage());
		}		
		return lista;
	}
		
		
			
		
	}
	
	
	
	
	
	
	

