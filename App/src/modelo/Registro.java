package modelo;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;

import javax.swing.JOptionPane;

import varios.Datos;

public class Registro {

	public Integer id;
	public Integer id_clase;
	public Integer id_cliente;
	public String fecha_entrada;
	public String hora;
	
	private Registro(Integer id,Integer idclas,Integer idcli,String f,String h){
		this.id=id;
		this.id_clase= idclas;
		this.id_cliente=idcli;
		this.fecha_entrada=f;
		this.hora=h;
		
	}
	public int delete() {
		return Registro.delete(this.id);
	}
	public static int delete(int i) {
		Conexion.open();
		PreparedStatement stm;
		try {
			stm=Conexion.con.prepareStatement("delete clientela_registro from clientela_registro where id=?");
			stm.setInt(1, i);
			stm.executeUpdate();
			stm.close();
			return 0;
		} catch (SQLException e) {
			if (Datos.debug) e.printStackTrace();
			Datos.mError(+e.getErrorCode()+" - "+e.getLocalizedMessage());
			return e.getErrorCode();
		}
		
	}
	public static Registro create (Integer idclas,Integer idcli,String f,String h) {
		Conexion.open();
		PreparedStatement insercion;
		ResultSet respuesta;
		try {
			insercion=Conexion.con.prepareStatement("insert into clientela_registro(id_clase,id_cliente,fecha_entrada,hora) values (?,?,?,?) ", Statement.RETURN_GENERATED_KEYS);
			insercion.setInt(1,idclas);
			insercion.setInt(2,idcli);
			insercion.setString(3,f);
			insercion.setString(4,h);
			insercion.executeUpdate();
			respuesta=insercion.getGeneratedKeys();
		
			
			//Integer ID=respuesta.getInt(1);
			/* recogeremos el id y cargaremos el cliente */
			if (respuesta.next()) return Registro.load(respuesta.getInt(1));
			

		} catch (SQLException e) {
			if (Datos.debug) e.printStackTrace();
			Datos.mError(+e.getErrorCode()+" - "+e.getLocalizedMessage());
			
		}
		

		return null;
	}
	
	public static Registro load(Integer i) {
		Conexion.open();
		PreparedStatement stm;
		ResultSet respuesta;
	
		
		try {
			stm = Conexion.con.prepareStatement("select * from clientela_registro where id=?" );
			stm.setInt(1,i);
			respuesta=stm.executeQuery();
			if (respuesta.next()) {
				return new Registro(respuesta.getInt("id"),
								   respuesta.getInt("id_clase"),
								   respuesta.getInt("id_cliente"),
								   respuesta.getString("fecha_entrada"),								 
								   respuesta.getString("hora"));
				
			} 
			
			stm.close();
			
		} catch (SQLException e) {
			if (Datos.debug) e.printStackTrace();
			Datos.mError(+e.getErrorCode()+" - "+e.getLocalizedMessage());
		}
		
		return null;
		
	}
	public void update() {
		Conexion.open();
		PreparedStatement stm;
		ResultSet respuesta;
		try {
			stm=Conexion.con.prepareStatement("update clientela_registro  set  id_clase=?,id_cliente=?,fecha_entrada=?,hora=? where id=? ");
			stm.setInt(1,this.id_clase);
			stm.setInt(2,this.id_cliente);
			stm.setString(3,this.fecha_entrada);
			stm.setString(4,this.hora);
			stm.setInt(5,this.id);
			stm.executeUpdate();
			stm.close();
			
		} catch (SQLException e) {
			if (Datos.debug) e.printStackTrace();
			Datos.mError(+e.getErrorCode()+" - "+e.getLocalizedMessage());
		}
	}
	
	public static LinkedList<Registro> find (String idclas,Integer idcli,String f){
		Conexion.open();
		PreparedStatement stm;
		ResultSet respuesta;
		int x=1;
		LinkedList<Registro> lista= new LinkedList<Registro>();
		String consulta="Select * from clientela_registro join clases on id_clase=clases.id  where ";
		if(idclas!=null)  consulta+=" nombre like ? and ";
		if(idcli!=null)  consulta+=" id_cliente =? and ";
		if(f!=null) consulta+=" fecha_entrada like ? and ";
		//join clases on id_clase=id
		consulta+="1+1";
		
		try {
			stm=Conexion.con.prepareStatement(consulta);
			if(idclas!=null)  {stm.setString(x, idclas+"%");x++;}
			if(idcli!=null)  {stm.setInt(x, idcli);x++;}
			if(f!=null) {stm.setString(x, f+"%");x++;}
		
			
			respuesta=stm.executeQuery();
			while(respuesta.next()) {
				lista.add(new Registro(respuesta.getInt("id"),
								   respuesta.getInt("id_clase"),
								   respuesta.getInt("id_cliente"),
								   respuesta.getString("fecha_entrada"),								 
								   respuesta.getString("hora")));
			}
			stm.close();
		} catch (SQLException e) {
			if (Datos.debug) e.printStackTrace();
			Datos.mError(+e.getErrorCode()+" - "+e.getLocalizedMessage());
		}		
		return lista;
	}
		
		
			
		
	}
	
	
	
	
	
	
	

