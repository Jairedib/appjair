package modelo;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

import varios.Datos;

public class Conexion {
	public static String bd;
	public static String login;
	public static String pass;
	public static String host;
	public static String url=null;
	
	public static Connection con=null;

	
	public static void open() {
		
		try {
			if ((con==null) || (con.isClosed())) {
				if (url==null) cargarPropiedades();
				url="jdbc:mysql://"+host+"/"+bd+"?useSSL=false";
				Class.forName("com.mysql.jdbc.Driver");
				con=DriverManager.getConnection(url, login, pass);
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("No se puede conectar!!!");
		}
		
	}
	
	public static void cargarPropiedades() {
		Properties prop=new Properties();
		InputStream input=null;
		try {
			input=new FileInputStream("config.properties");
			prop.load(input);
			bd=		prop.getProperty("bbdd");
			login=	prop.getProperty("usuario");
			host=	prop.getProperty("host");
			pass=	prop.getProperty("password");
			
		} catch (Exception e) {
			System.out.println("Falta archivo de configuración!!");
		}
		
	}
	public static void guardarpropiedades() {
		Properties prop=new Properties();
		OutputStream output = null;
		
		try {
			output = new FileOutputStream("config.properties");
			
			prop.setProperty("bbdd", bd);
			prop.setProperty("usuario", login);
			prop.setProperty("host", host);
			prop.setProperty("pass", pass);
			
try {
	prop.store(output,"Datos de configuración de la base de datos");
} catch (IOException e) {
	
	e.printStackTrace();
}
		} catch (FileNotFoundException e) {
			Datos.mError("error al conectar");
			
			
		}
		
		
		
	}
	

	public static void close() {
		try {
			if (!con.isClosed()) {
				con.close();
			}
		} catch (Exception e) {
			
		}
	}
	
	
}
