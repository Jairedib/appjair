package varios;

import javax.swing.JOptionPane;

public class Datos {
	
	public static boolean debug=true;
	public static void mError(String m) {
		

	JOptionPane.showMessageDialog(null,"error:"+m,"error", JOptionPane.ERROR_MESSAGE);

	}
	
	public static void minfo(String m) {
		JOptionPane.showMessageDialog(null,"informacion:"+m,"informacion", JOptionPane.INFORMATION_MESSAGE);

		}
	public static int mConfirmacion(String m) {
		return JOptionPane.showConfirmDialog(null, m, "Confirmación", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
	}

}
