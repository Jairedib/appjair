package varios;

import java.awt.BorderLayout;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JToolBar;

public class MiJpanel extends JPanel {
	public JButton btnbuscar ;
	public JButton btnNuevo ;
	public MiJpanel() {
		setVisible(true);
		revalidate();
		repaint();
		
		setLayout(new BorderLayout(0, 0));
		JToolBar toolBar = new JToolBar();
		this.add(toolBar, BorderLayout.NORTH);
		
		btnbuscar = new JButton("Buscar");
		toolBar.add(btnbuscar);
		
		btnNuevo = new JButton("Nuevo");
		toolBar.add(btnNuevo);
		JScrollPane scrollPane = new JScrollPane();
		this.add(scrollPane);
	
		
	}
	

}
